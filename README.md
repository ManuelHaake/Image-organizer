 # images_to_csv.sh
 
This script combines the commands **find** and **exiftool** to write all images found in given folder(s) in a csv file
including their path, filename and resolution (separate for x and y).
In fact the script scans all files in the folder(s). If the file has no
resolution, it will be marked as "unknown". The resolution should also work 
for video-files.

**Note:** This is probably not the most elegant solution to get the csv, but it gets the job done.

# search_images.sd

This script combines the commands **find** and **exiftool** to quickly search for specific
images within folder an print out their path and resolution
 
If run without parameters at startup, the user will be asked for the search term.
It is also possible to run it the search term at startup by running `bash search_images.sh keyword`.
