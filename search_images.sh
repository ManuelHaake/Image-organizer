#!/bin/bash

# This script combines the commands find and exiftool to quickly search for specific
# images within folder an print out their path and resolution
# 
# If run without parameters at startup, the user will be asked for the search term.
# It is also possible to run it the search term at startup --> bash search_images.sh keyword
# 
# Modify the folder(s) to your needs.
#
 
# define one or more folders to search in
#folder="/home/usr/example/folder1"
folder="/home/usr/example/folder1 /home/usr/example/folder2 /usr/local/bin"

search() {
    clear
    echo ""
    echo "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
    find $folder -iname \*$1\* -exec exiftool -filename -ImageWidth -ImageHeight -filesize -s  {} +
    echo "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
    echo ""
    echo "Cancel with CTRL+c"
    echo ""
    echo ""
    read -p "Type a string oder substring to search for: " suchstring
}

if [ -z "$1" ]; then
    read -p "Type a string oder substring to search for: " suchstring
else
    search $1
fi

while :
    do
        search $suchstring
    done
