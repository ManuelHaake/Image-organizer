#!/bin/bash

# This script combines the commands find and exiftool to write all images found in given folder(s) in a csv file
# including their path, filename and resolution (separate for x and y).
# In fact the script scans all files in the folder(s). If the file has no
# resolution, it will be marked as "unknown". The resolution should also work 
# for video-files.
#
# You can specify your folder(s) and the separator for the csv-file in the
# variables on top of the script.
#
# Note: This is probably not the most elegant solution to get the csv, but it gets the job done.

# Modify folder and separator to your needs
folder="/home/usr/example/folder1 /home/usr/example/folder2 /usr/local/bin"
csv_separator=";"

# Files and tempfile
sourcefile=$(pwd)"/sourcefile.txt"
tempfile=$(pwd)"/temp.txt"
outputfile=$(pwd)"/images.txt"

echo ""
echo "searching for files in the given folder(s):"
echo ""
echo -e $folder | tr " " "\n"
echo ""
echo "This may take some time, depending on the amount of files in your folder(s)"

# Search files within the given folder(s)
find $folder -iname \*""\* -exec exiftool -filename -ImageWidth -ImageHeight -filesize -s  {} + > $sourcefile

# make some replacements
sed -i 's/======== //' $sourcefile
sed -i 's/FileName                        : /'$csv_separator' FileName: /' $sourcefile
sed -i 's/ImageWidth                      : /'$csv_separator' ImageWidth: /' $sourcefile
sed -i 's/ImageHeight                     : /'$csv_separator' ImageHeight: /' $sourcefile
sed -i 's/FileSize                        : /'$csv_separator' FileSize: /' $sourcefile

echo "Done"

# remove lines beginning with whitespace (these are infolines from exiftool find which are not needed)
sed -i '/^ /d' $sourcefile

# for files without a resolution (other then images) set the resolution to "unknown"
lineold=""
content=""

echo "Analyze files without a given resolution. This may take some time, depending on the amount of files in your folder(s)"
echo "For really big collections maybe >15min, so be patient."

while read line; do
	# reading each line
	content=$content"\n"$line
    
    # if line for FileName is followed by line for FileSize no res is given -> set res to unknown
	if [[ "$line" == *"FileSize"* ]] && [[ "$lineold" == *"FileName"* ]]; then
		content=$content"\n; ImageWidth: unkown"
		content=$content"\n; ImageHeight: unkown"
	fi
	# remember last line
	lineold=$line
done < $sourcefile

# write content to tempfile
echo -e $content > $tempfile

# remove first line from tempfile
sed -i '1d' $tempfile

# remove sourcefile file as it is not needed anymore
rm $sourcefile

# format to .csv file by deleting every newline-character except every 5th
awk 'ORS=NR%5?" ":"\n"' $tempfile > $outputfile

# remove tempfile as it is not needed anymore
rm $tempfile

# insert Header in first line
sed -i '1 i\Path; FileName; ImageWidth; ImageHeight; FileSize' $outputfile

# replace the headers from each line as they are now given on the top of the file
sed -i 's/FileName: //g' $outputfile
sed -i 's/ImageWidth: //g' $outputfile
sed -i 's/ImageHeight: //g' $outputfile
sed -i 's/FileSize: //g' $outputfile

echo ""
echo "Done!"
echo "You can find the list of your images in the file $outputfile"
echo ""
